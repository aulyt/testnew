/*
Navicat SQLite Data Transfer

Source Server         : database
Source Server Version : 32200
Source Host           : :0

Target Server Type    : SQLite
Target Server Version : 32200
File Encoding         : 65001

Date: 2019-07-17 12:07:47
*/

PRAGMA foreign_keys = OFF;

-- ----------------------------
-- Table structure for "main"."user"
-- ----------------------------
DROP TABLE "main"."user";
CREATE TABLE user (id_user INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name_user VARCHAR(20) NOT NULL, lastname_user VARCHAR(30) NOT NULL, mail_user VARCHAR(50), birthday_user DATE NOT NULL);

-- ----------------------------
-- DELETE all
-- ----------------------------
DELETE
FROM user;

-- ----------------------------
-- DELETE one
-- ----------------------------
DELETE 
FROM user
WHERE id_user = 1;

-- ----------------------------
-- INSERT all
-- ----------------------------
INSERT INTO user (name_user, lastname_user, mail_user, birthday_user)
VALUES ('Anton', 'Ulytskiy', 'baton9012@gmail.com', '25.09.1997'),
				('Bogdan', 'Doben', 'bogdan@gmail.com', '06.06.1999');
				
-- ----------------------------
-- INSERT one
-- ----------------------------
INSERT INTO user (name_user, lastname_user, mail_user, birthday_user)
VALUES ('Anton', 'Ulytskiy', 'baton9012@gmail.com', '25.09.1997');

-- ----------------------------
-- SELECT all
-- ----------------------------
SELECT id_user, name_user, lastname_user, mail_user, birthday_user
FROM user;

-- ----------------------------
-- SELECT one
-- ----------------------------
SELECT id_user, name_user, lastname_user, mail_user, birthday_user
FROM user
WHERE id_user = 1;

-- ----------------------------
-- UPDATE all
-- ----------------------------
UPDATE user
SET	name_user = 'Bogdan',
		lastname_user = 'Duben',
		mail_user = 'bogdan@gmail.com'

-- ----------------------------
-- UPDATE one
-- ----------------------------		
UPDATE user
SET name_user = 'Bogdan',
		lastname_user = 'Duben',
		mail_user = 'bogdan@gmail.com'
WHERE id_user = 1;