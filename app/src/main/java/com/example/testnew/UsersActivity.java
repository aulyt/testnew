package com.example.testnew;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.os.Bundle;

public class UsersActivity extends AppCompatActivity{

    public static final String USER_ID = "user_id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_fragment);
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.user_fragment);
        if (fragment == null){
            fragment = new UserListFragment();
            fragmentManager.beginTransaction().add(R.id.user_fragment, fragment).commit();
        }
    }
}