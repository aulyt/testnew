package com.example.testnew;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;
import com.example.testnew.model.Pet;
import com.example.testnew.model.TypePet;
import com.example.testnew.model.User;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

public class TestActivity extends AppCompatActivity {

    private UserLab mUserLab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        mUserLab = UserLab.get(this);
        Button insertButton = findViewById(R.id.insert_button);
        insertButton.setOnClickListener(view -> {
            List<User> users = fillDataUser();
            List<TypePet> typePets = fillDataTypePet();
            List<Pet> pets = fillDataPet();
            int rowUser = mUserLab.addUsers(users);
            int rowTypePet = mUserLab.addTypePets(typePets);
            int rowPet = mUserLab.addPets(pets);
            String stringToast = "user " + rowUser + " type pet " + rowTypePet + " pet " + rowPet;
            Toast toast = Toast.makeText(getApplicationContext(), stringToast, Toast.LENGTH_SHORT);
            toast.show();
        });
        Button deleteButton = findViewById(R.id.delet_button);
        deleteButton.setOnClickListener(view -> {
            int rowUser = mUserLab.deleteUsers();
            int rowPet = mUserLab.deletePets();
            int rowTypePet = mUserLab.deleteTypePets();
            String stringToast = "user " + rowUser + " type pet " + rowTypePet + " pet " + rowPet;
            Toast toast = Toast.makeText(getApplicationContext(), stringToast, Toast.LENGTH_SHORT);
            toast.show();
        });
    }

    public static Intent newIntent(Context packageContext){
        return new Intent(packageContext, TestActivity.class);
    }

    private List<Pet> fillDataPet(){
        int random = new Random().nextInt(10);
        List<Pet> pets = new ArrayList<>(random);
        for (int i = 0; i <= random; i++){
            Pet pet = new Pet();
            pet.setPetName("pet name"+i);
            pet.setBirthdayPet(Calendar.getInstance().getTime());
            pets.add(pet);
        }
        return pets;
    }

    private List<TypePet> fillDataTypePet(){
        List<TypePet> typePets = new ArrayList<>();
        String[] typePetString = {"cat", "dog", "horse", "parrot", "hamster", "raccoon"};
        for (int i = 0; i< typePetString.length; i++){
            TypePet typePet = new TypePet();
            typePet.setNameTypePet(typePetString[i]);
            typePets.add(typePet);
        }
        return typePets;
    }

    private List<User> fillDataUser(){
        int random = new Random().nextInt(10);
        List<User> users = new ArrayList<>(random);
        for (int i = 0; i <= random; i++) {
            User user = new User();
            user.setName("name user" + i);
            user.setLastName("lastname user" + i);
            user.setMail("mail user" + i);
            user.setBirthday(Calendar.getInstance().getTime());
            users.add(user);
        }
        return users;
    }
}
