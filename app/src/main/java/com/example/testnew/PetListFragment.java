package com.example.testnew;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.testnew.model.PetModelList;
import java.util.List;
import static com.example.testnew.UsersActivity.USER_ID;

public class PetListFragment extends Fragment {

    private PetListAdapter mPetListAdapter;
    private RecyclerView mRecyclerView;
    private UserLab mUserLab;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserLab = UserLab.get(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_pet, container, false);
        mRecyclerView = view.findViewById(R.id.list_pet);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    private void updateUI(){
        Bundle arg  = getActivity().getIntent().getExtras();
        int id = (int) arg.get(USER_ID);
        List<PetModelList> petModelLists = mUserLab.getPets(id);
        if (mPetListAdapter == null) {
            mPetListAdapter = new PetListAdapter(petModelLists, getActivity());
        } else {
            mPetListAdapter.setPets(petModelLists);
            mPetListAdapter.notifyDataSetChanged();
        }
        mRecyclerView.setAdapter(mPetListAdapter);
    }
}
