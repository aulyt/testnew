package com.example.testnew;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.testnew.database.UserBaseHelper;
import com.example.testnew.database.UserCursorWrapper;
import com.example.testnew.database.UserDBSchema.PetTable;
import com.example.testnew.database.UserDBSchema.TypePetTable;
import com.example.testnew.database.UserDBSchema.UserTable;
import com.example.testnew.model.Pet;
import com.example.testnew.model.PetModelList;
import com.example.testnew.model.TypePet;
import com.example.testnew.model.User;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class UserLab {
    private static UserLab sUserLab;

    private SQLiteDatabase mDatabase;

    public static UserLab get(Context context){
        if (sUserLab == null){
            sUserLab = new UserLab(context);
        }
        return sUserLab;
    }

    private UserLab(Context context){
        mDatabase = new UserBaseHelper(context.getApplicationContext()).getWritableDatabase();
    }

    public int addUsers(List<User> users){
        int affectedRows = 0;
        StringBuilder sql = new StringBuilder(" insert into users (name_user, lastname_user, mail_user,birthday_user) values ");
        for (int i = 0; i < users.size(); i++) {
            sql.append(insertValuesStringUser(users, i));
            if (i == users.size() - 1 || i % 499 == 498){
                sql.append(";");
                mDatabase.execSQL(sql.toString());
                affectedRows += affectedRowsCounter(affectedRows);
                sql.replace(0, sql.length(), " insert into users (name_user, lastname_user, mail_user,birthday_user) values ");
            } else {
                sql.append(",");
            }
        }
        return affectedRows;
    }

    public int addPets(List<Pet> pets){
        String sql = "insert into pet (name_pet, birthday_pet, id_user_pet, id_type_pet_pet) values ";
        int affectedRows = 0;
        for (int i = 0; i < pets.size(); i++){
            sql += insertValuesStringPet(pets, i);
            if ( i == pets.size()-1 || i % 499 == 498){
                sql += ";";
                mDatabase.execSQL(sql);
                affectedRows += affectedRowsCounter(affectedRows);
                sql = "insert into pet (name_pet, birthday_pet, id_user_pet, id_type_pet_pet) values ";
            } else {
                sql += ",";
            }
        }
        return affectedRows;
    }

    public int addTypePets(List<TypePet> typePets){
        String sql = "insert into type_pet (name_type_pet) values ";
        int affectedRows = 0;
        for (int i = 0; i < typePets.size(); i++){
            TypePet typePet = typePets.get(i);
            sql += "('" + typePet.getNameTypePet() + "')";
            sql += ((i == typePets.size()-1) ? ";" : ",");
        }
        mDatabase.execSQL(sql);
        affectedRows += affectedRowsCounter(affectedRows);
        return affectedRows;
    }

    public int deleteUsers(){
        return mDatabase.delete(UserTable.TABLE_NAME, null, null);
    }

    public int deletePets(){
        return mDatabase.delete(PetTable.TABLE_NAME, null, null);
    }

    public int deleteTypePets(){
        return mDatabase.delete(TypePetTable.TABLE_NAME, null, null);
    }

    public List<User> getUsers(){
        List<User> users = new ArrayList<>();

        try (UserCursorWrapper userCursorWrapper = queryUsers(null, null, null)) {
            userCursorWrapper.moveToFirst();
            while (!userCursorWrapper.isAfterLast()) {
                users.add(userCursorWrapper.getUser());
                userCursorWrapper.moveToNext();
            }
        }

        return users;
    }

    public List<User> getIdUsers(){
        List<User> users = new ArrayList<>();

        try (UserCursorWrapper userCursorWrapper = queryUsers(new String[] {UserTable.Cols.ID_USER}, null, null)){
            userCursorWrapper.moveToFirst();
            while (!userCursorWrapper.isAfterLast()){
                users.add(userCursorWrapper.getIdUser());
                userCursorWrapper.moveToNext();
            }
        }
        return users;
    }

    public List<PetModelList> getPets(int id){
        List<PetModelList> petModelLists = new ArrayList<>();
        try (UserCursorWrapper userCursorWrapper = queryPets(new String[]{Integer.toString(id)})) {
            userCursorWrapper.moveToFirst();
            while (!userCursorWrapper.isAfterLast()) {
                petModelLists.add(userCursorWrapper.getPet());
                userCursorWrapper.moveToNext();
            }
        }
        return petModelLists;
    }

//    public List<TypePet> getTypePets(){
//        List<TypePet> typePets = new ArrayList<>();
//        try (UserCursorWrapper userCursorWrapper = queryTypePet( new String[] {TypePetTable.Cols.NAME_TYPE_PET}, null, null)){
//            userCursorWrapper.moveToFirst();
//            while (!userCursorWrapper.isAfterLast()){
//                typePets.add(userCursorWrapper.getTypePet());
//                userCursorWrapper.moveToNext();
//            }
//        }
//        return typePets;
//    }

    public List<TypePet> getIdTypePet(){
        List<TypePet> typePets = new ArrayList<>();
        try(UserCursorWrapper userCursorWrapper = queryTypePet(new String[] {TypePetTable.Cols.ID_TYPE_PET} , null, null)){
            userCursorWrapper.moveToFirst();
            while (!userCursorWrapper.isAfterLast()){
                typePets.add(userCursorWrapper.getIdTypePet());
                userCursorWrapper.moveToNext();
            }
        }
        return typePets;
    }

    private UserCursorWrapper queryPets(String[] whereArgs){

        Cursor cursor = mDatabase.rawQuery(
            "SELECT "
                        + UserTable.TABLE_NAME + "." + UserTable.Cols.NAME_USER + " ||  ' ' || "
                        + UserTable.TABLE_NAME + "." + UserTable.Cols.LASTNAME_USER + " as fullUserName, "
                        + UserTable.TABLE_NAME + "." + UserTable.Cols.MAIL_USER + ", "
                        + PetTable.TABLE_NAME + "." + PetTable.Cols.NAME_PET + ", "
                        + PetTable.TABLE_NAME + "." + PetTable.Cols.BIRTHDAY_PET + ", "
                        + PetTable.TABLE_NAME + "." + PetTable.Cols.ID_PET + ", "
                        + TypePetTable.TABLE_NAME + "." + TypePetTable.Cols.NAME_TYPE_PET
                + " FROM " + PetTable.TABLE_NAME
                + " JOIN " + UserTable.TABLE_NAME
                        + " on " + UserTable.TABLE_NAME + "." + UserTable.Cols.ID_USER + " = " + PetTable.TABLE_NAME + "." + PetTable.Cols.ID_USER
                + " JOIN " + TypePetTable.TABLE_NAME
                        + " on " +  TypePetTable.TABLE_NAME + "." + TypePetTable.Cols.ID_TYPE_PET + " = " + PetTable.TABLE_NAME + "." + PetTable.Cols.ID_TYPE_PET
                + " WHERE " +UserTable.TABLE_NAME + "." + UserTable.Cols.ID_USER + " = ? ;"
                        , whereArgs);
        return new UserCursorWrapper(cursor);
    }

    private UserCursorWrapper queryTypePet(String[] cols, String whereClause, String[] whereArgs){
        Cursor cursor = mDatabase.query(
                TypePetTable.TABLE_NAME,
                cols,
                whereClause,
                whereArgs,
                null,
                null,
                null
                );

        return new UserCursorWrapper(cursor);
    }

    private UserCursorWrapper queryUsers(String[] cols ,String whereClause, String[] whereArgs){
        Cursor cursor = mDatabase.query(
                UserTable.TABLE_NAME,
                cols,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );
        return new UserCursorWrapper(cursor);
    }

    private int affectedRowsCounter(int rowsCounter){
        Cursor res = mDatabase.rawQuery("select changes() 'affectedRows'", null);
        res.moveToFirst();
        rowsCounter += res.getInt(res.getColumnIndex("affectedRows"));
        return rowsCounter;
    }

    private String insertValuesStringUser(List<User> users, int interator){
        return "('" + users.get(interator).getName() + "', '" + users.get(interator).getLastName() + "', '" + users.get(interator).getMail() + "', '" + users.get(interator).getBirthday() + "')";
    }

    private String insertValuesStringPet(List<Pet> pets, int iterator){
        List<TypePet> idTypePets = getIdTypePet();
        int randomIDTypePet = new Random().nextInt(idTypePets.size());
        List<User> idUsers = getIdUsers();
        int randomIDUser = new Random().nextInt(idUsers.size());
        return  "('" + pets.get(iterator).getPetName() + "', '" + pets.get(iterator).getBirthdayPet() + "', "
                + "(select id_user from users where id_user = "+ idUsers.get(randomIDUser).getMId() + ") , "
                + "(select id_type_pet from type_pet where id_type_pet = " + idTypePets.get(randomIDTypePet).getIdTypePet() + "))";
    }
}
