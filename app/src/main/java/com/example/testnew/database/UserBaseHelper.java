package com.example.testnew.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.example.testnew.database.UserDBSchema.PetTable;
import com.example.testnew.database.UserDBSchema.TypePetTable;
import com.example.testnew.database.UserDBSchema.UserTable;

public class UserBaseHelper extends SQLiteOpenHelper {

    private static final int VERSION_DB = 1;
    private static final String DATABASE_NAME = "usersBase.db";

    public UserBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION_DB);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table " + TypePetTable.TABLE_NAME + "(" +
                TypePetTable.Cols.ID_TYPE_PET + " integer primary key autoincrement, " +
                TypePetTable.Cols.NAME_TYPE_PET + " text not null)"
        );

        sqLiteDatabase.execSQL("create table " + UserTable.TABLE_NAME + "(" +
                UserTable.Cols.ID_USER + " integer primary key autoincrement, " +
                UserTable.Cols.NAME_USER + " text not null, " +
                UserTable.Cols.LASTNAME_USER + " text not null, " +
                UserTable.Cols.MAIL_USER + " text, " +
                UserTable.Cols.BIRTHDAY_USER + " date not null)"
        );

        sqLiteDatabase.execSQL("create table " + PetTable.TABLE_NAME + "(" +
                PetTable.Cols.ID_PET + " integer primary key autoincrement, " +
                PetTable.Cols.NAME_PET + " text not null, " +
                PetTable.Cols.BIRTHDAY_PET + " text not null, " +
                PetTable.Cols.ID_USER + " integer not null, " +
                PetTable.Cols.ID_TYPE_PET + " integer not null, " +
                "foreign key (" + PetTable.Cols.ID_USER + ") references " + UserTable.TABLE_NAME + "(" + UserTable.Cols.ID_USER + "), " +
                "foreign key (" + PetTable.Cols.ID_TYPE_PET + ") references " + TypePetTable.TABLE_NAME + "(" + TypePetTable.Cols.ID_TYPE_PET + ")" +
                ")"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    }
}
