package com.example.testnew.database;

public class UserDBSchema {

    public static final class TypePetTable {
        public static final String TABLE_NAME = "type_pet";

        public static final class Cols {
            public static final String ID_TYPE_PET = "id_type_pet";
            public static final String NAME_TYPE_PET = "name_type_pet";
        }
    }

    public  static final class UserTable {
        public static final String TABLE_NAME = "users";

        public static final class Cols {
            public static final String ID_USER = "id_user";
            public static final String NAME_USER = "name_user";
            public static final String LASTNAME_USER = "lastname_user";
            public static final String MAIL_USER = "mail_user";
            public static final String BIRTHDAY_USER = "birthday_user";
        }
    }

    public static final class PetTable{
        public static final String TABLE_NAME = "pet";

        public static final class Cols {
            public static final String ID_PET = "id_pet";
            public static final String NAME_PET = "name_pet";
            public static final String BIRTHDAY_PET = "birthday_pet";
            public static final String ID_USER = "id_user_pet";
            public static final String ID_TYPE_PET = "id_type_pet_pet";
        }
    }
}
