package com.example.testnew.database;

import android.database.Cursor;
import android.database.CursorWrapper;
import com.example.testnew.database.UserDBSchema.PetTable;
import com.example.testnew.database.UserDBSchema.TypePetTable;
import com.example.testnew.database.UserDBSchema.UserTable;
import com.example.testnew.model.PetModelList;
import com.example.testnew.model.TypePet;
import com.example.testnew.model.User;
import java.util.Date;

public class UserCursorWrapper extends CursorWrapper {

    public UserCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public User getIdUser(){
        int idInt = getInt(getColumnIndex(UserTable.Cols.ID_USER));

        User user = new User(idInt);
        user.setMId(idInt);

        return user;
    }

    public User getUser(){
        int idInt = getInt(getColumnIndex(UserTable.Cols.ID_USER));
        String nameUserString = getString(getColumnIndex(UserTable.Cols.NAME_USER));
        String lastnameUserStrung = getString(getColumnIndex(UserTable.Cols.LASTNAME_USER));
        String mailUserString = getString(getColumnIndex(UserTable.Cols.MAIL_USER));
        long birthdayUserLong = getLong(getColumnIndex(UserTable.Cols.BIRTHDAY_USER));

        User user = new User(idInt);
        user.setName(nameUserString);
        user.setLastName(lastnameUserStrung);
        user.setMail(mailUserString);
        user.setBirthday(new Date(birthdayUserLong));

        return user;
    }

    public TypePet getIdTypePet(){
        int idTypePetInt = getInt(getColumnIndex(TypePetTable.Cols.ID_TYPE_PET));

        TypePet typePet = new TypePet();
        typePet.setIdTypePet(idTypePetInt);

        return typePet;
    }

    public TypePet getTypePet(){
        String nameTypePetString = getString(getColumnIndex(TypePetTable.Cols.NAME_TYPE_PET));

        TypePet typePet = new TypePet();
        typePet.setNameTypePet(nameTypePetString);

        return typePet;
    }

    public PetModelList getPet(){

        String fullNameUserPetString = getString(getColumnIndex("fullUserName"));
        String mailUserPetString = getString(getColumnIndex(UserTable.Cols.MAIL_USER));
        String namePetString = getString(getColumnIndex(PetTable.Cols.NAME_PET));
        long birthdayPetLong = getLong(getColumnIndex(PetTable.Cols.BIRTHDAY_PET));
        long idPetLong = getLong(getColumnIndex(PetTable.Cols.ID_PET));
        String nameTypePetString = getString(getColumnIndex(TypePetTable.Cols.NAME_TYPE_PET));

        PetModelList petModelList = new PetModelList();
        petModelList.setFullUserNamePet(fullNameUserPetString);
        petModelList.setUserMailPet(mailUserPetString);
        petModelList.setPetName(namePetString);
        petModelList.setPetBirthday(new Date(birthdayPetLong));
        petModelList.setIdPet(idPetLong);
        petModelList.setNameTypePet(nameTypePetString);

        return petModelList;
    }
}
