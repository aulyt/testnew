package com.example.testnew.model;

import java.util.Date;

public class Pet {

    private long mIdPet;
    private String mPetName;
    private Date mBirthdayPet;
    private long mIdUser;
    private long mIdTypePet;

    public Pet(long id){
        mIdPet = id;
    }

    public Pet (){
        mIdPet = 0;
        mBirthdayPet = new Date();
    }

    public long getIdPet() {
        return mIdPet;
    }

    public void setIdPet(long idPet) {
        mIdPet = idPet;
    }

    public String getPetName() {
        return mPetName;
    }

    public void setPetName(String petName) {
        mPetName = petName;
    }

    public long getIdUser() {
        return mIdUser;
    }

    public void setIdUser(long idUser) {
        mIdUser = idUser;
    }

    public long getIdTypePet() {
        return mIdTypePet;
    }

    public void setIdTypePet(long idTypePet) {
        mIdTypePet = idTypePet;
    }

    public Date getBirthdayPet() {
        return mBirthdayPet;
    }

    public void setBirthdayPet(Date birthdayPet) {
        mBirthdayPet = birthdayPet;
    }
}
