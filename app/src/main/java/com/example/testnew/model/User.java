package com.example.testnew.model;

import java.util.Date;

public class User {
    private int mId;
    private String mName;
    private String mLastName;
    private String mMail;
    private Date mBirthday;

    public User(int id) {
        mId = id;
    }

    public User() {
        mId = 0;
        mBirthday = new Date();
    }

    public int getMId() {
        return mId;
    }

    public void setMId(int mId) {
        this.mId = mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public String getMail() {
        return mMail;
    }

    public void setMail(String mail) {
        mMail = mail;
    }

    public Date getBirthday() {
        return mBirthday;
    }

    public void setBirthday(Date birthday) {
        mBirthday = birthday;
    }
}
