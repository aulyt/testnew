package com.example.testnew.model;

import java.util.Date;

public class PetModelList {

    private long mIdPet;
    private String mfullUserName;
    private String mUserMailPet;
    private String mPetName;
    private Date mPetBirthday;
    private String mNameTypePet;

    public String getFullUserNamePet() {
        return mfullUserName;
    }

    public void setFullUserNamePet(String userNamePet) {
        mfullUserName = userNamePet;
    }

    public String getUserMailPet() {
        return mUserMailPet;
    }

    public void setUserMailPet(String userMailPet) {
        mUserMailPet = userMailPet;
    }

    public String getPetName() {
        return mPetName;
    }

    public void setPetName(String petName) {
        mPetName = petName;
    }

    public Date getPetBirthday() {
        return mPetBirthday;
    }

    public void setPetBirthday(Date petBirthday) {
        mPetBirthday = petBirthday;
    }

    public String getNameTypePet() {
        return mNameTypePet;
    }

    public void setNameTypePet(String nameTypePet) {
        mNameTypePet = nameTypePet;
    }

    public long getIdPet() {
        return mIdPet;
    }

    public void setIdPet(long idPet) {
        mIdPet = idPet;
    }
}
