package com.example.testnew.model;

public class TypePet {
    private long idTypePet;
    private String NameTypePet;

    public long getIdTypePet() {
        return idTypePet;
    }

    public void setIdTypePet(long idTypePet) {
        this.idTypePet = idTypePet;
    }

    public String getNameTypePet() {
        return NameTypePet;
    }

    public void setNameTypePet(String nameTypePet) {
        NameTypePet = nameTypePet;
    }
}
