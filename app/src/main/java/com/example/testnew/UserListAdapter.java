package com.example.testnew;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Adapter;

import com.example.testnew.model.User;

import java.util.List;

public class UserListAdapter extends Adapter<UserListAdapter.UserHolder> {

    private List<User> mUsers;
    private LayoutInflater mLayoutInflater;
    private OnUserClickListener mOnUserClickListener;

    public UserListAdapter(Context context, List<User> users, OnUserClickListener onUserClickListener) {
        mUsers = users;
        mOnUserClickListener = onUserClickListener;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public UserHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserHolder(mLayoutInflater, parent);
    }

    @Override
    public void onBindViewHolder(@NonNull UserHolder holder, int position) {
        holder.bind(mUsers.get(position), mOnUserClickListener);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mUsers.size();
    }

    public static class UserHolder extends RecyclerView.ViewHolder {
        private User mUser;
        private TextView mFullNameUser;
        private TextView mBirthday;
        private TextView mMail;
        public UserHolder(LayoutInflater inflater, ViewGroup parent){
            super(inflater.inflate(R.layout.item_user, parent, false));
            mFullNameUser = itemView.findViewById(R.id.name_user);
            mBirthday = itemView.findViewById(R.id.birthday_user);
            mMail = itemView.findViewById(R.id.email_user);
        }

        public void bind(User user, OnUserClickListener onUserClickListener){
            mUser = user;
            String fullNameUser = mUser.getName()+" "+ mUser.getLastName();
            mFullNameUser.setText(fullNameUser);
            mMail.setText(mUser.getMail());
            mBirthday.setText(mUser.getBirthday().toString());
            itemView.setOnClickListener(view -> onUserClickListener.onUserClick(getAdapterPosition()));
        }
    }

    public void setUsers(List<User> users){
        mUsers = users;
    }

    public interface OnUserClickListener {
        void onUserClick(int position);
    }
}
