package com.example.testnew;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.testnew.model.PetModelList;
import java.util.List;

public class PetListAdapter extends RecyclerView.Adapter<PetListAdapter.PetHolder> {

    private LayoutInflater mLayoutInflater;
    private List<PetModelList> mPetModelLists;

    public PetListAdapter(List<PetModelList> petModelLists, Context context){
        mPetModelLists = petModelLists;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public PetHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PetHolder(mLayoutInflater, parent);
    }

    @Override
    public void onBindViewHolder(@NonNull PetHolder holder, int position) {
        holder.bind(mPetModelLists.get(position));
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mPetModelLists.size();
    }

    public static class PetHolder extends RecyclerView.ViewHolder{
        private PetModelList mPetModelList;
        private TextView mNamePet;
        private TextView mTypePet;
        private TextView mBirthdayPet;
        private TextView mNameUserPet;
        private TextView mMailUserPet;

        public PetHolder(LayoutInflater inflater, ViewGroup parent){
            super(inflater.inflate(R.layout.item_pet, parent, false));
            mNamePet = itemView.findViewById(R.id.name_pet);
            mTypePet = itemView.findViewById(R.id.type_pet);
            mBirthdayPet = itemView.findViewById(R.id.birthday_pet);
            mNameUserPet = itemView.findViewById(R.id.name_user_pet);
            mMailUserPet = itemView.findViewById(R.id.mail_user_pet);
        }
        public void bind(PetModelList petModelList){
            mPetModelList = petModelList;
            mNamePet.setText(mPetModelList.getPetName());
            mTypePet.setText(mPetModelList.getNameTypePet());
            mBirthdayPet.setText(mPetModelList.getPetBirthday().toString());
            mNameUserPet.setText(mPetModelList.getFullUserNamePet());
            mMailUserPet.setText(mPetModelList.getUserMailPet());
        }
    }

    public void setPets(List<PetModelList> petModelLists){
        mPetModelLists = petModelLists;
    }
}