package com.example.testnew;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testnew.model.User;

import java.util.List;

import static com.example.testnew.UsersActivity.USER_ID;

public class UserListFragment extends Fragment implements UserListAdapter.OnUserClickListener {

    private RecyclerView mRecyclerView;
    private List<User> mUsers;
    private UserLab mUserLab;
    private UserListAdapter mUserListAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserLab = UserLab.get(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_activity, container, false);
        mRecyclerView = view.findViewById(R.id.listview_users);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.user_avtivity_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_test_activity:
                Intent intent = TestActivity.newIntent(getContext());
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateUI(){
        mUsers = mUserLab.getUsers();
        if (mUserListAdapter == null){
            mUserListAdapter = new UserListAdapter(getActivity(), mUsers, this);
        } else {
            mUserListAdapter.setUsers(mUsers);
            mUserListAdapter.notifyDataSetChanged();
        }
        mRecyclerView.setAdapter(mUserListAdapter);
    }

    @Override
    public void onUserClick(int position) {
        Intent intent = PetActivity.newIntent(getContext());
        int id = mUsers.get(position).getMId();
        intent.putExtra(USER_ID, id);
        startActivity(intent);
    }
}
